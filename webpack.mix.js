const mix = require('laravel-mix');

mix.scripts([
    'public/asset/js/jquery-2.1.1.js',
    'public/asset/js/bootstrap.min.js',
    'public/asset/js/plugins/metisMenu/jquery.metisMenu.js',
    'public/asset/js/plugins/slimscroll/jquery.slimscroll.min.js',
    'public/asset/js/inspinia.js',
    'public/asset/js/plugins/pace/pace.min.js',
    'public/asset/js/plugins/toastr/toastr.min.js',
    'public/asset/js/plugins/datapicker/bootstrap-datepicker.js',
    'public/asset/js/plugins/sweetalert/sweetalert.min.js',
], 'public/js/dashboard.js')
.styles([
    'public/asset/css/animate.css',
    'public/asset/css/style.css',
    'public/asset/css/plugins/datapicker/datepicker3.css',
    'public/asset/css/plugins/sweetalert/sweetalert.css',
    'public/asset/css/plugins/toastr/toastr.min.css',
    'public/asset/css/custom.css',
    ], 'public/css/dashboard.css');
