<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        $rules['name'] = 'required';
        $rules['dob'] = 'required';
        $rules['email'] = 'required';
        $rules['password'] = 'required';
        $rules['contact_no'] = 'required';
        $rules['father_name'] = 'required';
        $rules['mother_name'] = 'required';
        $rules['marital_status'] = 'required';
        $rules['bank_name'] = 'required';
        $rules['bank_branch'] = 'required';
        $rules['account_number'] = 'required';
        $rules['ifsc_code'] = 'required';

        if($this->marital_status == 'married')
        {
            $rules['spouse_name'] = 'required';
            $rules['anniversary'] = 'required';
        }

        return $rules;
    }
}
