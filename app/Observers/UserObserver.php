<?php

namespace App\Observers;

use App\Mail\UserCreated;
use App\Models\User;
use App\Notifications\UserCreatedNotification;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;

class UserObserver
{
    public function creating(User $user)
    {
        if(request()->file)
        {
            $file_name = time().'.'.request()->file->extension();
            $user->photo = $file_name; // Save file name to database
        }
    }

    /**
     * Handle the User "created" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function created(User $user)
    {
        if(request()->file)
        {
            $path = public_path('user-uploads/users');
            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $user->photo);
        }

        $request = request();

        // Mail::send(['html' => 'emails.welcome'], ['user' => $request], function($message) use($request) {
        //     $message->from('demo@example.com');
        //     $message->to($request->email);
        //     $message->subject('Congrats, your account has been created.');
        // });

        $user->notify(new UserCreatedNotification($request));
    }

    public function updating(User $user)
    {
        if(request()->file) {

            // Old file delete code
            $path = public_path('user-uploads/users/');
            $this->deleteFile($path.$user->photo);

            $file_name = time().'.'.request()->file->extension();
            $user->photo = $file_name; // Save file name to database
        }
    }

    /**
     * Handle the User "updated" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        if(request()->file) {

            $path = public_path('user-uploads/users/');

            if (!file_exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            }
            request()->file->move($path, $user->photo);
        }
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        $path = public_path('user-uploads/users/');

        // Old file delete code
        $this->deleteFile($path.$user->photo);
    }

    /**
     * Handle the User "restored" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }

    public function deleteFile($path)
    {
        File::delete($path);
    }

}
